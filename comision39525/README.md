// eliminar cód repetido
// variables declaradas nunca usadas
// importaciones declaradas nunca usadas
// código comentado no tiene que estar
// ningun console.log debe estar excepto errores

// identar bien el cód 
// prohibido mezclar idiomas en el cód 

# Nombre app
## _The Last Markdown Editor, Ever_

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Dillinger is a cloud-enabled, mobile-ready, offline-storage compatible,
AngularJS-powered HTML5 Markdown editor.

- Type some Markdown on the left
- See HTML in the right
- ✨Magic ✨