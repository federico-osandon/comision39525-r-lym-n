import { doc, getDoc, getFirestore } from "firebase/firestore"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { TextComponent5, TextComponent6, TextComponent7 } from "../../clases/clase11/ComponenteEjemplosCondicionales"
import { gFetch, gFetchOne } from "../../utils/gFetch"

import ItemDetail from "../ItemDetail/ItemDetail"


const ItemDetailContainer = () => {
    const [ producto, setProducto ] =  useState({})
    const { idProducto } = useParams()
    // console.log(idProducto)

    useEffect(()=>{        
        const db = getFirestore() 
        const query = doc(db, 'items', idProducto)
        getDoc(query)
        .then(resp => setProducto( { id: resp.id, ...resp.data() } ))
    }, [])

  // mocks con id 
    return (
        <div 
        // className="border border-5 border-danger  m-3"
        >        
            {/* <TextComponent >
            </TextComponent> */}
            {/* <TextComponent7 otro='mt-5' /> */}
            
            <ItemDetail producto={producto} />


            
        </div>
    )
}

export default ItemDetailContainer

// condition ? 'verdadero' : 'false' -> if else
// condition  && 'verdadero
// condition ||  condition2 or es un o