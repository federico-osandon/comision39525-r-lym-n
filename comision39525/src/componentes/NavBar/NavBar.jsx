import { Link, NavLink } from 'react-router-dom';

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import CartWidget from '../CartWidget/CartWidget'

const categorias = [ 
    {id: '1', nombre: 'Gorras', idCategoria: 'gorras', descripcion: 'Esto es gorras'},
    {id: '2', nombre: 'Remeras', idCategoria: 'remeras', descripcion: 'Esto es remeras'},
    {id: '3', nombre: 'Pantalones', idCategoria: 'pantalones,', descripcion: 'Esto es remeras'}
]

const NavBar = () => {
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                {/* <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand> */}
                <NavLink to='/' >React-Bootstrap</NavLink>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="me-auto">
                { categorias.map(cate => <NavLink to={`/categoria/${cate.idCategoria}`} className={({ isActive })=> isActive  ? 'btn btn-primary':'btn btn-outline-primary' } >{cate.nombre}</NavLink>)}
                   
                    
                </Nav>
                <Nav>
                    {/* <Nav.Link href="#deets">🛒</Nav.Link> */}
                    {/* <Nav.Link eventKey={2} href="#memes">
                    Dank memes
                    </Nav.Link> */}
                    <Link to='/cart'>
                        <CartWidget />
                    </Link>
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default NavBar