import { useState, useEffect } from 'react'

import './App.css'


// Anatomía 
// props 
// estados  // useState() ( hook ) -> React 
// ciclo de vida

// -La primera vez que se ejecuta app se llama mount (montaje)

// acualizaciones:
// los componentes se re render ( ejecutar ) de nuevo con:
//  - ejecuto un evento
//  - cambio en un estado 
//  - cambio en una props 



function App(props) { 
    const [ count , setCount ]  = useState(0)// [estado, funcionCambiarEstado]
    const [ fecha, setFecha ] = useState(Date())
    const [bool, setBool ] = useState(false)


    useEffect(()=>{
        // acciones que se ejutan como efecto sec. 
        console.log('se ejecuta siempre - 1')
        
    }) 

    useEffect(()=>{
        // acciones que se ejutan como efecto sec. 
        console.log('llamada a las api fect() - 2')
        
    }, [])

    useEffect(()=>{
        // acciones que se ejutan como efecto sec. 
        console.log('solo cuando cambie bool- 3')
        
    }, [bool, count]) 
    
    
    const hanldeCount = () => {
        
        setCount( count + 1 )
    } // count = count + 1 // count += 1 // count ++
    const hanldeBool = () => {
        
        setBool( !bool )
    } // count = count + 1 // count += 1 // count ++
    
    
    console.log('Antes del rendering 4')
    return (
        <>
            <h1>Click: { count }</h1>
            <h1>FyH: { fecha }</h1>
            <button onClick={ hanldeCount }>Click</button>
            <button onClick={ hanldeBool }>bool</button>
        </>
    )
}

export default App



